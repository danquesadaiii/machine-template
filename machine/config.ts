import { MachineConfig } from 'xstate'

import { Context, StateSchema, MachineEvents } from './types'

export const config: MachineConfig<Context, StateSchema, MachineEvents> = {
  id: '<machine_id>>',
  initial: 'loading',
  states: {
    loading: {},
    ready: {},
  },
}
