import { AnyStateNodeDefinition, StateNodeDefinition } from 'xstate'
import { Context, MachineEvents } from '../'

export interface StateSchema {
  states: {
    loading: StateNodeDefinition<Context, AnyStateNodeDefinition, MachineEvents>
    ready: StateNodeDefinition<Context, AnyStateNodeDefinition, MachineEvents>
  }
}
