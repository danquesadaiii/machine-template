import { AnyEventObject } from 'xstate'

import { IRecord } from './'

export interface SUCCESS {
  type: 'SUCCESS'
  payload: IRecord
}

export interface ERROR {
  type: 'ERROR'
  payload: IRecord
}

export type MachineEvents = AnyEventObject
